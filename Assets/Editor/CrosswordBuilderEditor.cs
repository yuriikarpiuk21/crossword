﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(CrosswordBuilder))]
public class CrosswordBuilderEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        GUILayout.Label("Use * - for space.");
        var myScript = (CrosswordBuilder)target;
        if(GUILayout.Button("Build Crossword"))
        {
            myScript.Generate();
        }
    }
}
