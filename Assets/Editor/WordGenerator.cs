﻿using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

[CustomEditor(typeof(WordBuilderScript))]
public class WordGenerator : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        WordBuilderScript myScript = (WordBuilderScript)target;
        if(GUILayout.Button("Build Word"))
        {
            myScript.BuildWord();
        }
        if(myScript._child)
        {
            myScript._child.GetComponent<RectTransform>().sizeDelta = myScript.GetComponent<RectTransform>().sizeDelta;
        }
        if(myScript._child && myScript.IsBuilded && GUILayout.Button("Done"))
        {
            DestroyImmediate(myScript);
        }
    }
}
