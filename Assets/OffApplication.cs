﻿using UnityEngine;
using System.Collections;

public class OffApplication : MonoBehaviour
{

    public void Off()
    {
        Application.Quit();
    }
}
