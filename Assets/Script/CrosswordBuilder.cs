﻿using UnityEngine;
using System.Collections.Generic;
using System.Text;
using UnityEngine.UI;

public class CrosswordBuilder : MonoBehaviour
{
    [SerializeField][Multiline(15)] private string Words;
    [SerializeField] private GameObject _component;
    [HideInInspector] public List<Letter> _allLetter;
    private List<string> _allWords;
    public void Generate()
    {
        if(!_component)
        {
            Debug.LogError("Missing var: \"Component\"");
            return;
        }
        ClearOld();

        _allWords = MultilineToList(Words);
        var compRect = _component.GetComponent<RectTransform>();
        var size = SetSize();
        var startPos = new Vector2((-size.x / 2) + compRect.sizeDelta.x /2, (size.y / 2) - compRect.sizeDelta.x / 2);

        GenerateWords(_allWords, startPos);
        SetTransition();
        CreateController();
    }
    private void GenerateWords(List<string> allWords, Vector2 startPos)
    {
        var currentPos = startPos;
        var compRect = _component.GetComponent<RectTransform>();
        for(int i = 0; i < allWords.Count; i++)
        {
            var isFirstLetter = true;
            for (int j = 0; j < allWords[i].Length; j++)
            {

                if(allWords[i][j] == '*')
                {
                    currentPos.x += compRect.sizeDelta.x;
                    continue;
                }
                var obj = Instantiate(_component, currentPos, Quaternion.identity) as GameObject;
                if (isFirstLetter)
                {
                    obj.GetComponentInChildren<WordNumber>().Number = (i + 1).ToString();
                }
                var letter = obj.AddComponent<Letter>();
                letter.LetterChar = allWords[i][j];

                _allLetter.Add(letter);
                obj.transform.SetParent(transform);
                obj.GetComponent<RectTransform>().anchoredPosition = currentPos;
                obj.transform.localScale = Vector3.one;
                currentPos.x += compRect.sizeDelta.x;
                isFirstLetter = false;
            }
            currentPos.x = startPos.x;
            currentPos.y -= compRect.sizeDelta.y;
        }
    }
    private void SetTransition()
    {
        for(int i = 0; i < _allLetter.Count; i++)
        {
            if(!(i + 1 > _allLetter.Count - 1))
            {
                _allLetter[i].NextField = _allLetter[i + 1].GetComponent<InputField>();
            }
            if(i != 0)
            {
                _allLetter[i].PreviousField = _allLetter[i - 1].GetComponent<InputField>();
            }
        }
    }
    private void ClearOld()
    {
        if(_allLetter != null)
        {
            _allLetter.ForEach(x => { if(x != null) DestroyImmediate(x.gameObject); });
            _allLetter.Clear();
        }
    }
    private Vector2 SetSize()
    {
        return GetComponent<RectTransform>().sizeDelta =
             new Vector2(
                 Words.GetMultiLineLength(0) * _component.GetComponent<RectTransform>().sizeDelta.x,
                 Words.GetMultiLineLength(1) * _component.GetComponent<RectTransform>().sizeDelta.y
                 );
    }
    public void CreateController()
    {
        var old = GetComponent<CrosswordController>();
        if(old)
            DestroyImmediate(old);
        var controller = gameObject.AddComponent<CrosswordController>();
        for(int i = 0; i < _allWords.Count; i++)
        {
            _allWords[i] = _allWords[i].Replace("*", "");
        }

        controller.EditorInit(_allWords,_allLetter);
    }
    private List<string> MultilineToList(string str)
    {
        var list = new List<string>();
        StringBuilder buf = new StringBuilder();
        for(int i = 0; i < str.Length; i++)
        {
            if(Words[i] == '\n')
            {
                list.Add(buf.ToString());
                buf.Remove(0, buf.Length);
                continue;
            }
            buf.Append(str[i]);
            if(i == str.Length - 1)
                list.Add(buf.ToString());
        }
        return list;
    }
}