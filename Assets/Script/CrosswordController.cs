﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class CrosswordController : MonoBehaviour
{
    //private static CrosswordController _instance;
    /*public static CrosswordController Instance
    {
        get { return _instance; }
        private set { _instance = value; }
    }*/

    [SerializeField]
    private List<string> _allWords;
    [SerializeField]
    private List<Letter> _allLetter;
    public event System.EventHandler<System.EventArgs> CrosswordResolved;
    public Letter SelectedLetter;

    public void EditorInit(List<string> allWords, List<Letter> allLetter)
    {
        _allWords = allWords;
        _allLetter = allLetter;
    }

    public void Start()
    {
        Init();
    }

    public void Init()
    {
        CrosswordResolved += (o, e) => MessageBox.Show("Результат:", "Вирішено - вірно");

        _allLetter.ForEach(x => x.Init());
    }
    public void CheckAll()
    {
        for (int i = 0; i < _allLetter.Count; i++)
            if (!_allLetter[i].IsRight)
            {
                Debug.LogError(_allLetter[i].LetterChar);
                return;
            }

        Debug.LogError("Красава");
        if(CrosswordResolved != null)
        {
            CrosswordResolved(this, null);

        }
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Backspace))
        {
            if (!SelectedLetter)
            {
                return;
            }
            if(SelectedLetter.PreviousField != null)
            {
                SelectedLetter.PreviousField.Select();
            }
        }
    }

    /*private void Awake()
    {
        Instance = this;
    }*/
}
