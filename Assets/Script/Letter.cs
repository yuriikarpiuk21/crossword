﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using UnityEngine.EventSystems;

public class Letter : MonoBehaviour, ISelectHandler
{
    public char LetterChar;
    public InputField NextField;
    public InputField PreviousField;
    private InputField _currentField;
    public bool IsRight;

    public void Init()
    {
        LetterChar = LetterChar.ToString().ToUpper()[0];
        _currentField = GetComponent<InputField>();

        _currentField.onValueChanged.AddListener(OnValueChange);
    }

    private void OnValueChange(string arg0)
    {
        _currentField.text = _currentField.text.ToUpper();
        if(NextField)
        {
            NextField.Select();
        }

        Debug.LogError(1);
        if(arg0 == string.Empty)
            return;

        //
        Debug.LogError(2 + " " + _currentField.text + " "+ LetterChar);
        IsRight = (_currentField.text[0] == LetterChar) ? true : false;
        if(IsRight)
        {
            transform.parent.GetComponent<CrosswordController>().CheckAll();
            Debug.LogError(3);
        }
        Debug.LogError(4);
    }

    private bool IsEmpty()
    {
        return _currentField.text == "" ? true : false;
    }

    public void OnSelect(BaseEventData eventData)
    {
        transform.parent.GetComponent<CrosswordController>().SelectedLetter = this;
    }
}