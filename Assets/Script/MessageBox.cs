﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MessageBox : MonoBehaviour
{
    public static MessageBox Instance = null;
    public Text Title;
    public Text Message;

    public Color DefaultColor;

    public void OnDisable()
    {
        Reset();
    }    

    public void Reset()
    {
        Message.color = DefaultColor;
    }
    public static void Show(string title, string message, Color color)
    {
        if (!Instance)
        {
            Instance = FindObjectOfType<MessageBox>();

            Debug.LogError(GameObject.Find("Messagebox"));
        }
        Instance.gameObject.SetActive(true);
        Instance.Title.text = title;
        Instance.Message.text = message;
        Instance.Message.color = color;
    }

    public static void Show(string title, string message)
    {
        if (!Instance)
        {
            Instance = FindObjectOfType<MessageBox>();

            Debug.LogError(GameObject.Find("Messagebox"));
        }
        Instance.gameObject.SetActive(true);
        Instance.Title.text = title;
        Instance.Message.text = message;
    }
}
