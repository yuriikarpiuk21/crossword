﻿using System.Linq;
using UnityEngine.UI;

static class MyExtensions
{
    public static int GetMultiLineLength(this string str, int dimension)
    {
        int result = 0;
        int buffer = 0;
        if(dimension == 0)
        {
            for(int i = 0; i < str.Length; i++)
            {
                if(str[i] == '\n')
                {
                    if(buffer > result)
                        result = buffer;
                    buffer = 0;
                }
                else
                {
                    buffer++;
                    if(i == str.Length - 1)
                    {
                        if(buffer > result)
                            result = buffer;
                    }
                }
            }
        }
        else if(dimension == 1)
        {
            result = 1;
            for(int i = 0; i < str.Length; i++)
                if(str[i] == '\n')
                    result++;
        }
        return result;
    }
    public static Toggle GetActive(this ToggleGroup aGroup)
    {
        return aGroup.ActiveToggles().FirstOrDefault();
    }
}