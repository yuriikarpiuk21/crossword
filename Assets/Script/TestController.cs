﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class TestController : MonoBehaviour
{
    public ToggleGroup[] allToggleGroups;
    public Button CheckButton;
    public bool[] allAnswers;

    private void Start()
    {
        Init();
    }
    [ContextMenu("1")]
    public void Init()
    {
        CheckButton.onClick.AddListener(() =>
        {
            for (int i = 0; i < allToggleGroups.Length; i++)
            {
                var checkObj = allToggleGroups[i].GetComponent<CheckAnswer>();
                if (checkObj.CurrentToggle != checkObj.TrueToggle)
                {
                    MessageBox.Show("Результат:", "Невірно, спробуйте ще.", Color.red);
                    return;
                }
            }
            MessageBox.Show("Результат:", "Вірно, ви вирішили вірно усі питання.");
            Debug.LogError(!false);
        });

        allToggleGroups = GetComponentsInChildren<ToggleGroup>();
        for (int i = 0; i < allToggleGroups.Length; i++)
        {
            var toggles = allToggleGroups[i].GetComponentsInChildren<Toggle>();
            for (int j = 0; j < toggles.Length; j++)
            {
                Debug.Log(toggles[j]);
                var item = toggles[j];
                item.onValueChanged.AddListener(x =>
                {
                    if (item)
                    {
                        if (item.isOn)
                        {
                            Debug.Log(item);
                            item.transform.parent.GetComponent<CheckAnswer>().CurrentToggle = item;
                        }
                    }
                });
            }
        }

        allAnswers = new bool[GetComponentsInChildren<ToggleGroup>().Length];
    }
}
