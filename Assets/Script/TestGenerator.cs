﻿using UnityEngine;
using System.Collections.Generic;
using System.Xml;
using System.IO;

public class TestGenerator : MonoBehaviour
{
    public string TestName;

    public string Question;
    public List<Answer> Answers = new List<Answer>();
    [System.Serializable]
    public class Answer
    {
        public string AnswerStr;
        public bool IsTrue;
        public Answer(string answer, bool isTrue)
        {
            AnswerStr = answer;
            IsTrue = isTrue;
        }
    }

    void Start()
    {
        TextAsset a = Resources.Load<TextAsset>("Test");
        XmlDocument doc = new XmlDocument();
        doc.LoadXml(a.text);
        foreach (XmlNode noda in doc.DocumentElement)
        {
            if (TestName != noda.Attributes.Item(0).Value)
                continue;

            Question = noda.ChildNodes[0].Attributes.Item(0).Value;

            foreach (XmlNode item in noda.ChildNodes[0].ChildNodes)
            {
                bool isTrue = false;
                if (item.Attributes.Item(1) != null)
                    isTrue = true;
                Answers.Add(new Answer(item.Attributes.Item(0).Value, isTrue));


            }
        }
        //XmlParser.ReadXml_File();
    }
    /*
    class XmlParser
    {
        public static void ReadXml_File()
        {
            TextAsset text = Resources.Load<TextAsset>("Test");
            MemoryStream stream = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(text.text));
            XmlTextReader reader = new XmlTextReader(stream);
            while(reader.Read())
            {
                switch(reader.NodeType)
                {
                    case XmlNodeType.Element: // The node is an element.
                        Debug.Log("<" + reader.Name);
                        Debug.Log(">");
                        if(reader.AttributeCount > 0)
                        {
                            WriteReaderAttributes(reader);
                        }
                        //Debug.Indent();
                        break;
                    case XmlNodeType.Text: //Display the text in each element.
                        Debug.Log(reader.Value.ToString());
                        break;
                    case XmlNodeType.EndElement: //Display the end of the element.
                        //Debug.Unindent();
                        Debug.Log("</" + reader.Name);
                        Debug.Log(">");
                        break;
                    case XmlNodeType.Attribute:
                        Debug.Log("  Attribute ");
                        Debug.Log("</" + reader.Name + " = " + reader.Value.ToString());
                        Debug.Log(">");
                        break;
                }
            }
            //Console.ReadLine();
            return;
        }

        private static void WriteReaderAttributes(XmlTextReader reader)
        {
            int attributeCount = reader.AttributeCount;
            for(int i = 0; i < attributeCount; i++)
            {
                reader.MoveToAttribute(i);
                Debug.Log("  Attribute ");
                Debug.Log("</" + reader.Name + " = " + reader.Value.ToString());
                Debug.Log(">");
            }
        }
    }*/
}
