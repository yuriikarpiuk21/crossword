﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    [System.Serializable]
    public class PageGroup
    {
        public GameObject DefaultPage;
        public GameObject InfoPage;
        public GameObject CrosswordsPage;
        public GameObject TestPage;
        public GameObject WhoItPage;
        public GameObject PuzzlesPage;
    }
    public static UIController Instance;

    public MessageBox MessageBoxObj;
    public ScrollRect ScrollRectMain;
    public PageGroup[] AllPages;
    public GameObject CurrentPage;
    public int CurrentUnit = 0;
    public void Awake()
    {
        Instance = this;
        MessageBox.Instance = MessageBoxObj;
        ScrollRectMain = GetComponent<ScrollRect>();
        SetDefaultPage();
    }
    public void SetUnit(int index)
    {
        CurrentUnit = index;
        SetDefaultPage();
    }
    public void SetPage(GameObject page)
    {
        if (!page)
        {
            MessageBox.Show("Помилка", "Немає відповідної сторінки");
            return;
        }
        if (CurrentPage)
        {
            CurrentPage.SetActive(false);
        }
        Debug.LogError(page);
        CurrentPage = page;
        CurrentPage.SetActive(true);
        ScrollRectMain.content = page.GetComponent<RectTransform>();
    }
    public void SetDefaultPage()
    {
        SetPage(AllPages[CurrentUnit].DefaultPage);
    }
    public void SetInfoPage()
    {
        SetPage(AllPages[CurrentUnit].InfoPage);
    }
    public void SetCrosswordsPage()
    {
        SetPage(AllPages[CurrentUnit].CrosswordsPage);
    }
    public void SetTestPage()
    {
        SetPage(AllPages[CurrentUnit].TestPage);
    }
    public void SetWhoItPage()
    {
        SetPage(AllPages[CurrentUnit].WhoItPage);
    }
    public void SetPuzzlesPage()
    {
        SetPage(AllPages[CurrentUnit].PuzzlesPage);
    }
}