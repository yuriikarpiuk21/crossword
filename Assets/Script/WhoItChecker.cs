﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Linq;

public class WhoItChecker : MonoBehaviour
{
    InputField InputFieldObj;
    public string Answer;
    public string[] ElseAnswers;
    public Color ColorTrue;
    public Color ColorFalse;

    void Start()
    {
        InputFieldObj = GetComponent<InputField>();
        InputFieldObj.onEndEdit.AddListener(x =>
        {
            if (InputFieldObj.text.Equals(Answer, System.StringComparison.InvariantCultureIgnoreCase))
            {
                Debug.Log(true);
                transform.parent.GetComponent<Image>().color = ColorTrue;
            }
            else
            {
                if (ElseAnswers != null)
                    for (int i = 0; i < ElseAnswers.Length; i++)
                        if (InputFieldObj.text.Equals(ElseAnswers[i], System.StringComparison.InvariantCultureIgnoreCase))
                        {
                            transform.parent.GetComponent<Image>().color = ColorTrue;
                            return;
                        }
                
                transform.parent.GetComponent<Image>().color = ColorFalse;
            }
        });
    }

}
