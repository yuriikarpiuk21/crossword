﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class WordNumber : MonoBehaviour
{
    public string Number
    {
        get
        {
            return GetComponent<Text>().text;
        }
        set
        {
            GetComponent<Text>().text = value.ToString();
        }
    }
}
