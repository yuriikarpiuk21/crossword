﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WordBuilderScript : MonoBehaviour
{
    public string Word;
    public Direction LayoutDirection;
    [SerializeField]
    private GameObject _component;
    [SerializeField]
    private Vector3 _spawnPoint;
    [HideInInspector]
    public List<GameObject> _allLetter;
    [HideInInspector]
    public GameObject _child;
    public bool IsBuilded { get; private set; }

    public void BuildWord()
    {
        if(!_component)
        {
            Debug.LogError("Missing var: \"Component\"");
            return;
        }
        if(!_child)
        {
            _child = new GameObject("Child");
            _child.transform.SetParent(transform);
            var rect = _child.AddComponent<RectTransform>();

            rect.localPosition = Vector3.zero;
            rect.localScale = Vector3.one;
        }

        _child.GetComponent<RectTransform>().sizeDelta = GetComponent<RectTransform>().sizeDelta = 
             new Vector2(Word.Length * _component.GetComponent<RectTransform>().sizeDelta.x, 
             _component.GetComponent<RectTransform>().sizeDelta.y);

        if(_allLetter != null)
        {
            _allLetter.ForEach(x => DestroyImmediate(x));
            _allLetter.Clear();
        }
        ChangeDir(LayoutDirection);

        for(int i = 0; i < Word.Length; i++)
        {
            var obj = Instantiate(_component, _spawnPoint, Quaternion.identity) as GameObject;

            _allLetter.Add(obj);
            obj.transform.SetParent(_child.transform);
            obj.transform.localScale = Vector3.one;
        }
        IsBuilded = true;
    }
    private void ChangeDir(Direction dir)
    {
        HorizontalOrVerticalLayoutGroup layout = null;
        HorizontalOrVerticalLayoutGroup old = null;
        if(dir == Direction.Horizontal)
        {
            old = _child.GetComponent<VerticalLayoutGroup>();
            if(old) { DestroyImmediate(old); }
            if(!_child.GetComponent<HorizontalLayoutGroup>())
                layout = _child.AddComponent<HorizontalLayoutGroup>();
        }
        else
        {
            old = _child.GetComponent<HorizontalLayoutGroup>();
            if(old) { DestroyImmediate(old); }
            if(!_child.GetComponent<VerticalLayoutGroup>())
                layout = _child.AddComponent<VerticalLayoutGroup>();
        }
        if(layout)
        {
            layout.childForceExpandHeight = false;
            layout.childForceExpandWidth = false;
            layout.childAlignment = TextAnchor.MiddleCenter;
        }
    }
    public enum Direction
    {
        Horizontal,
        Vertical,
    }
}
